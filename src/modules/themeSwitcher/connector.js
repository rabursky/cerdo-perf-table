import { connect } from 'react-redux'
import { KEY } from './reducers'

const mapStateToProps = state => ({
  theme: state.get(KEY),
})

const mapDispatchToProps = dispatch => ({
  setTheme: theme => dispatch({ type: 'SET_THEME', theme }),
})

export default connect(mapStateToProps, mapDispatchToProps)
