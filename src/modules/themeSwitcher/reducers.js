import { createReducer } from '@utilitywarehouse/cerdo-sdk'

export const KEY = 'THEME_SWITCHER'
export const handlers = {
  SET_THEME: (state, { theme }) => theme.toLowerCase(),
}
export const reducer = createReducer('light', handlers)
export default { [KEY]: reducer }
