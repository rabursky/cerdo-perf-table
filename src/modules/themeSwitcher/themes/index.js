import lightTheme from './lightTheme'
import darkTheme from './darkTheme'

export default {
  light: lightTheme,
  dark: darkTheme,
}