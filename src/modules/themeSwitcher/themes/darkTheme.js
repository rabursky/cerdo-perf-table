export default {
  components: {
    Table: {
      styles: {
        highlighted: {
          backgroundColor: 'aliceBlue',
          border: 0,
        },
        th: {
          color: 'gray',
          backgroundColor: 'white',
          padding: '0.5em 0.25em',
          borderWidth: 1,
          borderColor: 'gray',
          borderStyle: 'solid',
        },
        thead: {
          backgroundColor: 'gray',
        },
        tr: {
          textAlign: 'center',
        },
        td: {
          borderWidth: 1,
          borderStyle: 'solid',
          borderColor: 'gray',
        },
        edit: {
          color: 'blue',
          fontSize: 11,
        },
        word: {
          fontWeight: 'bold',
        },
        words: {
          textAlign: 'center',
          fontFamily: 'serif',
          fontStyle: 'italic',
        }
      }
    }
  },
}
