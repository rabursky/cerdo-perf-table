import { ThemeProvider } from '@utilitywarehouse/cerdo-sdk'
import { mapProps, compose } from 'recompose';
import { connect } from 'react-redux'

import ThemeSwitcherReducers, { KEY } from './reducers'

import themes from './themes'

const ConnectedThemeProvider = compose(
  connect(state => ({ theme: state.get(KEY) })),
  mapProps(({ theme, ...props }) => ({
    variables: themes[theme].variables,
    presets: themes[theme].presets,
    components: themes[theme].components,
    ...props,
  })),
)(ThemeProvider)

export default () => ({
  redux: {
    reducers: ThemeSwitcherReducers,
  },
  providers: [ConnectedThemeProvider],
})
