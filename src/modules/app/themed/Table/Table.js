import React from 'react'
import { themedComponent, StyleSheet } from '@utilitywarehouse/cerdo-sdk'
import PropTypes from 'prop-types'

import map from 'lodash/map'
import take from 'lodash/take'

class TableHead extends React.PureComponent {
  render() {
    return (
      <thead style={this.props.theadStyle}>
        <tr>
          <th style={this.props.thStyle}>Word</th>
          <th style={this.props.thStyle}>Words</th>
          <th style={this.props.thStyle}>Value 1</th>
          <th style={this.props.thStyle}>Value 2</th>
          <th style={this.props.thStyle}>Value 3</th>
          <th style={this.props.thStyle}>Value 4</th>
          <th style={this.props.thStyle}>Value 5</th>
          <th style={this.props.thStyle}>Value 6</th>
          <th style={this.props.thStyle}>Value 7</th>
          <th style={this.props.thStyle}>Tools</th>
        </tr>
      </thead>
    )
  }
}

class TableRow extends React.PureComponent {
  static propTypes = {
    item: PropTypes.string.isRequired,
    trStyle: PropTypes.object,
    editStyle: PropTypes.object,
  }

  render() {
    const { item, trStyle, tdStyle, editStyle, wordStyle, wordsStyle } = this.props
    const num = item.length
    return (
      <tr style={trStyle}>
        <td style={{ ...tdStyle, ...wordStyle }}>{item}</td>
        <td style={{ ...tdStyle, ...wordsStyle}}>{`${item} ${item} ${item} ${item} ${item} ${item} ${item}`}</td>
        <td style={tdStyle}>{num}</td>
        <td style={tdStyle}>{num}</td>
        <td style={tdStyle}>{num}</td>
        <td style={tdStyle}>{num}</td>
        <td style={tdStyle}>{num}</td>
        <td style={tdStyle}>{num}</td>
        <td style={tdStyle}>{num}</td>
        <td style={{ ...tdStyle, ...editStyle }}>edit</td>
      </tr>
    )
  }
}

class TableBody extends React.PureComponent {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.string).isRequired,
    limit: PropTypes.number.isRequired,
  }

  render() {
    const { data, limit, ...rowProps } = this.props
    return (
      <tbody>
        {map(take(data, limit), (item) => (
          <TableRow key={item} item={item} {...rowProps}/>
        ))}
      </tbody>
    )
  }
}

const Table = ({ data, limit, highlight, styles }) => {
  // Something to be worked on...
  const tableStyle = StyleSheet.flatten(styles.table)
  const highlightedStyle = StyleSheet.flatten(styles.highlighted)
  const thStyle = StyleSheet.flatten(styles.th)
  const theadStyle = StyleSheet.flatten(styles.thead)
  const trStyle = StyleSheet.flatten(styles.tr)
  const tdStyle = StyleSheet.flatten(styles.td)
  const editStyle = StyleSheet.flatten(styles.edit)
  const wordStyle = StyleSheet.flatten(styles.word)
  const wordsStyle = StyleSheet.flatten(styles.words)

  return (
    <table style={tableStyle}>
      <colgroup>
        <col />
        <col />
        <col style={highlight === 1 ? highlightedStyle : undefined} />
        <col style={highlight === 2 ? highlightedStyle : undefined} />
        <col style={highlight === 3 ? highlightedStyle : undefined} />
        <col style={highlight === 4 ? highlightedStyle : undefined} />
        <col style={highlight === 5 ? highlightedStyle : undefined} />
        <col style={highlight === 6 ? highlightedStyle : undefined} />
        <col style={highlight === 7 ? highlightedStyle : undefined} />
        <col />
      </colgroup>
      <TableHead thStyle={thStyle} theadStyle={theadStyle}/>
      <TableBody 
        data={data}
        limit={limit}
        trStyle={trStyle}
        tdStyle={tdStyle}
        editStyle={editStyle}
        wordStyle={wordStyle}
        wordsStyle={wordsStyle}
      />
    </table>
  )
}

Table.propTypes = {
  styles: PropTypes.object.isRequired,
  data: PropTypes.arrayOf(PropTypes.string).isRequired,
  highlight: PropTypes.number.isRequired,
  limit: PropTypes.number.isRequired,
}

const withTableStyles = themedComponent({
  name: 'Table',
  stylesToProps: 'styles',
  styles: {
    table: {
      flex: 1,
      borderCollapse: 'collapse',
    },
    highlighted: {
      borderWidth: 2,
      borderColor: 'red',
      borderStyle: 'solid',
    },
    th: {
      color: 'white',
      fontSize: 12,
      padding: '0.75em 0.5em',
    },
    thead: {
      backgroundColor: 'gray',
    },
    tr: {
      borderWidth: 0,
      borderBottomWidth: 1,
      borderTopWidth: 1,
      borderStyle: 'solid',
      borderColor: 'gray',
      textAlign: 'center',
    },
    td: {
      fontSize: 14,
      padding: '0.5em 0.25em',
    },
    edit: {
      color: 'blue',
      fontSize: 11,
    },
    word: {
      fontWeight: 'bold',
    },
    words: {
      textAlign: 'left',
    }
  }
})

export default withTableStyles(Table)
