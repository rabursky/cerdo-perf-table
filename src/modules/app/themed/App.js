import React from 'react'
import { View, Text, themedComponent } from '@utilitywarehouse/cerdo-sdk'
import PropTypes from 'prop-types'
import { withStateHandlers } from 'recompose'

import Table from './Table'
import { words } from './data'
import withThemeSwitch from 'modules/themeSwitcher/connector'

export const ThemeContext = React.createContext({})

const withAppState = withStateHandlers(
  { limit: 100, highlight: 0 },
  {
    onLimitChange: () => (limit) => ({ limit }),
    onHighlightChange: () => (highlight) => ({ highlight }),
  },
)

const ThemeSwitcherUI = ({ setTheme }) => (
  <select
        onChange={(event) => setTheme(event.target.value)}
      >
    <option value="light">light</option>
    <option value="dark">dark</option>
  </select>
)

const ThemeSwitcher = withThemeSwitch(ThemeSwitcherUI)

const withAppStyles = themedComponent({
  name: 'App',
  styles: {
    container: {
      padding: 30,
      flex: 1,
    },
    header: {
      paddingBottom: 60,
      flexDirection: 'row',
      justifyContent: 'center',
    }
  },
  stylesToProps: 'styles',
})

const App = ({
  highlight,
  limit,
  onHighlightChange,
  onLimitChange,
  styles,
}) => (
  <View style={styles.container}>
    <View style={styles.header}>
      <Text>Number of items: </Text>
      <select
        value={String(limit)}
        onChange={(event) => onLimitChange(+event.target.value)}
      >
        <option value="0">0</option>
        <option value="10">10</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="200">200</option>
        <option value="500">500</option>
        <option value="1000">1000</option>
      </select>
      <Text> Highlight value col: </Text>
      <select
        value={String(highlight)}
        onChange={(event) => onHighlightChange(+event.target.value)}
      >
        <option value="0">none</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
      </select>
      <Text> Active theme: </Text>
      <ThemeSwitcher />
    </View>
    <Table data={words} limit={limit} highlight={highlight} />
  </View>
)

App.propTypes = {
  highlight: PropTypes.number.isRequired,
  limit: PropTypes.number.isRequired,
  onHighlightChange: PropTypes.func.isRequired,
  onLimitChange: PropTypes.func.isRequired,
  styles: PropTypes.any,
}

export default withAppStyles(withAppState(App))
