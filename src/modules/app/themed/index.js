import App from './App'

export default () => ({
    providers: [App]
})