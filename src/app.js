import React from 'react'
import {
  create,
  Provider,
} from '@utilitywarehouse/cerdo-sdk'

import AppModule from 'modules/app'
import ThemeSwitcherModule from 'modules/themeSwitcher'

export default function() {

  const instance = create(
    AppModule(),
    ThemeSwitcherModule(),
  )

  return {
    rootComponent: <Provider instance={instance} />,
  }
}
